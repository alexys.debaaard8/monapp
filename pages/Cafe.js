import React, { useState } from 'react';
import { Text, View, FlatList } from 'react-native';
import axios from 'axios';

const urlApi= "https://random-data-api.com/api/coffee/random_coffee?size=5";
export function Cafe(props) {

    const renderItem = ({ item }) => (
        <View style={{backgroundColor:"white", margin:10, padding:10, borderRadius:15}}>
            <Text>{item.blend_name}</Text>
            <Text>{item.origin}</Text>
        </View>
    );



  const [data, setData] = useState( 
    async () => {
      const result = await axios(urlApi);
      setData(result.data);
      console.log(result.data);
  });

  return (
    <View>
        <FlatList
            data={data}
            renderItem={renderItem}
        />
    </View>
  );
}