import * as React from 'react';
import { Button, View, Text } from 'react-native';

export function NotificationsScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'yellow' }}>
      <Text>Bonjour, vous êtes sur la page des notifications</Text>
      <Button onPress={() => navigation.goBack()} title="Go back home" />
    </View>
  );
}
