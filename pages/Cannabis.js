import React, { useState } from 'react';
import { Text, View, StyleSheet, FlatList } from 'react-native';
import axios from 'axios';

const urlApi= "https://random-data-api.com/api/cannabis/random_cannabis?size=6";
export function Medic(props) {

    const renderItem = ({ item }) => (
        <View style={{backgroundColor:"pink", margin:10, padding:10}}>
            <Text>{item.brand}</Text>
            <Text>{item.medical_use}</Text>
            <Text>{item.type}</Text>
        </View>
    );



  const [data, setData] = useState( 
    async () => {
      const result = await axios(urlApi);
      setData(result.data);
      console.log(result.data);
  });

  return (
    <View>
        <FlatList
            data={data}
            renderItem={renderItem}
        />
    </View>
  );
}