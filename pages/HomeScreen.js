import * as React from 'react';
import { Button, View, Text } from 'react-native';
import { Medic } from './Cannabis';
import { Cafe } from './Cafe';

export function HomeScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'red' }}>
      <Text>Bonjour, vous êtes sur la page d'accueil</Text>
      <Button
        onPress={() => navigation.navigate('Notifications')}
        title="Go to notifications"
      />
      <Button 
        onPress={() => navigation.navigate('Test')}
        title= "Page test"
      />
      <Button title="Open drawer" onPress={() => navigation.openDrawer()} />
      <Button title="Toggle drawer" onPress={() => navigation.toggleDrawer()} />
      <Cafe />
    </View>
  );
}