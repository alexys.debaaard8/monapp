import * as React from 'react';
import {Text, View, Button } from 'react-native';

export function TestScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Details Screen</Text>
      <Button onPress={() => navigation.goBack()} title="Go back home" />


    </View>
  );
}