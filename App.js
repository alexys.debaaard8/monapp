import * as React from 'react';
import { createDrawerNavigator,DrawerContentScrollView, DrawerItemList, DrawerItem } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { HomeScreen } from './pages/HomeScreen';
import { NotificationsScreen } from './pages/NotificationScreen';
import { TestScreen } from './pages/TestScreen';

const Drawer = createDrawerNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="Home">
        <Drawer.Screen name="Home" component={HomeScreen} />
        <Drawer.Screen name="Notifications" component={NotificationsScreen} />
        <Drawer.Screen name="Test" component={TestScreen} />
        
      </Drawer.Navigator>
    </NavigationContainer>
  );
}
